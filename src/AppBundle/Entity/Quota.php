<?php
/**
 * Created by PhpStorm.
 * User: mleczakm
 * Date: 15.11.16
 * Time: 23:30
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Money\Money;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 *
 * @ORM\Table
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"year"},
 *     message = "quota_for_year_already_defined"
 * )
 */
class Quota
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var int
     *
     * @ORM\Column(type="integer", unique=true)
     */
    private $year;
    /**
     * @var Money
     *
     * @ORM\Column(type="money")
     */
    private $amount;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param int $year
     * @return Quota
     */
    public function setYear(int $year)
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return Money
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param Money $amount
     * @return Quota
     */
    public function setAmount(Money $amount)
    {
        $this->amount = $amount;
        return $this;
    }
}