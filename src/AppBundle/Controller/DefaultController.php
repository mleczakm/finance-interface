<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('default.html.twig');
    }

    public function usersListAction()
    {
        return $this->render('default.html.twig');
    }
}
