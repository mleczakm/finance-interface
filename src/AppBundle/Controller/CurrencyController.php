<?php
/**
 * Created by PhpStorm.
 * User: mleczakm
 * Date: 17.11.16
 * Time: 21:02
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CurrencyController extends Controller
{
    public function indexAction()
    {
        $currencies = $this->getParameter('tbbc_money.currencies');
        $main_currency = $this->getParameter('tbbc_money.reference_currency');

        $ratioProvider = $this->get('tbbc_money.ratio_provider.google');

        $currenciesWithRatio = [];
        foreach ($currencies as $currency) {
            if ($currency === $main_currency)
                $currenciesWithRatio[$currency] = 1;
            else
                $currenciesWithRatio[$currency] = $ratioProvider->fetchRatio($main_currency, $currency);
        }

        return $this->render('currency/index.html.twig', ['currenciesWithRatio' => $currenciesWithRatio]);
    }

}