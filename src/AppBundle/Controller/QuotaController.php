<?php
/**
 * Created by PhpStorm.
 * User: mleczakm
 * Date: 17.11.16
 * Time: 22:09
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Quota;
use AppBundle\Form\QuotaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class QuotaController extends Controller
{
    public function indexAction()
    {

        return $this->render('quota/index.html.twig', ['quotas' => $this->getDoctrine()->getManager()->getRepository(Quota::class)
            ->findBy([], ['year' => 'DESC'])]);
    }

    public function newAction(Request $request)
    {
        $quota = new Quota();
        $form = $this->createForm(QuotaType::class, $quota);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->addFlash('success', 'quota_add_success');

                $em = $this->getDoctrine()->getManager();
                $em->persist($form->getData());
                $em->flush();

                return $this->redirectToRoute('quotas');
            }
        }

        return $this->render('quota/new.html.twig', ['form' => $form->createView()]);
    }

    public function editAction(Request $request, Quota $quota)
    {
        $form = $this->createForm(QuotaType::class, $quota);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $this->addFlash('success', 'quota_edit_success');

                $em = $this->getDoctrine()->getManager();
                $em->flush();

                return $this->redirectToRoute('quotas');
            }
        }

        return $this->render('quota/new.html.twig', ['form' => $form->createView()]);
    }
}