<?php
/**
 * Created by PhpStorm.
 * User: mleczakm
 * Date: 15.11.16
 * Time: 21:32
 */

namespace AppBundle\Menu;


use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $translator = $this->container->get('translator');


        $menu = $factory->createItem('root');

        $menu->addChild('home', [
            'label' => $translator->trans('menu.label.home'),
            'route' => 'homepage'
        ]);

        $menu->addChild('entries', [
            'label' => $translator->trans('menu.label.entries'),
            'extras' => ['safe_label' => true]
        ]);

        $menu['entries']->addChild('all', [
            'label' => $translator->trans('menu.label.all_entries'),
            'route' => 'homepage'
        ]);

        $menu['entries']->addChild('mine', [
            'label' => $translator->trans('menu.label.mine_entries'),
            'route' => 'homepage'
        ]);

        $menu['entries']->addChild('new', [
            'label' => $translator->trans('menu.label.new_entries'),
            'route' => 'homepage'
        ]);

        $menu->addChild('users', [
            'label' => $translator->trans('menu.label.users'),
            'extras' => ['safe_label' => true]
        ]);

        $menu['users']->addChild('all', [
            'label' => $translator->trans('menu.label.all_users'),
            'route' => 'users'
        ]);

        $menu['users']->addChild('new', [
            'label' => $translator->trans('menu.label.new_users'),
            'route' => 'users'
        ]);

        $menu->addChild('quotas', [
            'label' => $translator->trans('menu.label.quotas'),
            'extras' => ['safe_label' => true]
        ]);

        $menu['quotas']->addChild('list', [
            'label' => $translator->trans('menu.label.quotas_list'),
            'route' => 'quotas'
        ]);

        $menu['quotas']->addChild('edit', [
            'label' => $translator->trans('menu.label.new_year_quota'),
            'route' => 'quota_new'
        ]);

        $menu->addChild('currencies', [
            'label' => $translator->trans('menu.label.currencies'),
            'extras' => ['safe_label' => true],
            'route' => 'currency',
        ]);

        return $menu;
    }
}